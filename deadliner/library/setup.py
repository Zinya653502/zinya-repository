from setuptools import setup, find_packages

setup(
    name='library',
    version='0.0.7',
    description='A simple application to manage your deadlines',
    packages=find_packages()
)
