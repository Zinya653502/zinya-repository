import os
import configparser


def create_config(db_path='', log_path='',
                  web_path='', test_path=''):
    """
    Creates config
    :param db_path: path to database
    :param log_path: path to log
    :param test_path: path to unittests database
    :param web_path: payh to web database
    :return:
    """
    config = configparser.ConfigParser()
    config.add_section("PATH")
    config.add_section("Session")
    config.add_section("Interface")
    config.set('PATH', 'LOG_PATH', log_path)
    config.set('PATH', 'DB_PATH', db_path)
    config.set('PATH', 'TEST_PATH', test_path)
    config.set('PATH', 'WEB_PATH', web_path)
    config.set('Interface', 'mode', 'CLI')
    config.set('Session', 'uid', '0')
    config.set('Session', 'login', '')

    with open(get_cfg_path(), 'w') as cfg:
        config.write(cfg)


def load_config():
    """
    Load configuration from config file. If there was no configuration file, it will be created
    :return config: console configuration with paths to db,log,etc.

    """
    config = configparser.ConfigParser()
    try:
        open(get_cfg_path(), 'r').close()
    except FileNotFoundError:
        create_config()
    config.read(get_cfg_path())
    return config


def path_establish(path):
    """
    Checks for a directory at the specified path. If the directory does not exist, it will be created
    :param path: path to check
    :return: None. Path will be created

    """
    if not os.path.exists(path):
        os.makedirs(path)


def get_cfg_path():

    config_folder = 'config'

    cfg_path = os.path.dirname(os.path.abspath(__file__))
    cfg_path = os.path.split(cfg_path)[0]
    cfg_path = os.path.join(cfg_path, config_folder)
    path_establish(cfg_path)
    cfg_path = os.path.join(cfg_path, 'config.ini')
    return cfg_path


def get_db_path():
    """
    Return path to database
    :return - path to database:

    """
    config = load_config()
    if config.get('Interface', 'mode') == 'TEST':
        db_path = config.get('PATH', 'TEST_PATH')
        if not db_path:
            db_path = os.path.dirname(os.path.abspath(__file__))
            db_path = os.path.split(db_path)[0]
            db_path = os.path.join(db_path, 'TEST.sqlite3')
            db_path = 'sqlite:///' + db_path

    elif config.get('Interface', 'mode') == 'CLI':
        db_path = config.get('PATH', 'DB_PATH')
        if not db_path:

            db_path = os.path.dirname(os.path.abspath(__file__))
            db_path = os.path.split(db_path)[0]
            db_path = os.path.join(db_path, 'CLI.sqlite3')
            db_path = 'sqlite:///' + db_path

    elif config.get('Interface', 'mode') == 'WEB':
        db_path = config.get('PATH', 'WEB_PATH')
        if not db_path:
            db_path = os.path.dirname(os.path.abspath(__file__))
            db_path = os.path.split(db_path)[0]
            db_path = os.path.join(db_path, 'WEB.sqlite3')
            db_path = 'sqlite:///' + db_path
    else:
        raise Exception
    return db_path


def get_django_path():
    """
    Return path to logging files
    :return: path to logging files

    """
    db_path = os.path.dirname(os.path.abspath(__file__))
    db_path = os.path.split(db_path)[0]
    db_path = os.path.join(db_path, 'WEB.sqlite3')
    return db_path


def get_log_path():
    """
    Return path to logging files
    :return: path to logging files

    """
    config = load_config()
    log_path = config.get('PATH', 'LOG_PATH')
    if not log_path:
        log_folder = 'logs'
        log_path = os.path.dirname(os.path.abspath(__file__))
        log_path = os.path.split(log_path)[0]
        log_path = os.path.join(log_path, log_folder)
        path_establish(log_path)
        log_path = os.path.join(log_path, 'log.log')
    return log_path


def session_update(uid, login):
    """
    Updates session info
    :param uid: int -- user ID
    :param login: str -- user login
    :return:
    """
    config = load_config()
    config.set('Session', 'uid', str(uid))
    config.set('Session', 'login', login)

    with open(get_cfg_path(), 'w') as cfg:
        config.write(cfg)


def session_read():
    """
    Returns session info
    :return: uid, login
    """
    config = load_config()
    uid = config['Session']['uid']
    login = config['Session']['login']
    return int(uid), login


def session_clear():
    """
    Clears session info
    :return: void
    """
    config = load_config()
    config.set('Session', 'uid', '0')
    config.set('Session', 'login', '')

    with open(get_cfg_path(), 'w') as cfg:
        config.write(cfg)


def turn_on_test_mode():
    """
    Return path to database
    :return - path to database:

    """

    config = load_config()
    config.set('Interface', 'mode', 'TEST')

    with open(get_cfg_path(), 'w') as cfg:
        config.write(cfg)


def turn_on_cli_mode():
    """
    Return path to database
    :return - path to database:

    """
    config = load_config()
    config.set('Interface', 'mode', 'CLI')

    with open(get_cfg_path(), 'w') as cfg:
        config.write(cfg)


def turn_on_web_mode():
    """
    Return path to database
    :return - path to database:

    """
    config = load_config()
    config.set('Interface', 'mode', 'WEB')

    with open(get_cfg_path(), 'w') as cfg:
        config.write(cfg)
