import library.controllers.config_controller as config_ctrl
import library.controllers.models_controller as models_ctrl
import library.controllers.storage_controller as storage_ctrl
import library.models.error as error
import datetime
import unittest


class TestTasks(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        config_ctrl.turn_on_test_mode()

    def setUp(self):

        models_ctrl.user_create("User 1", "1111")
        models_ctrl.user_create("User 2", "2222")
        models_ctrl.user_create("User 3", "3333")
        models_ctrl.user_create("User 4", "4444")
        models_ctrl.task_create("User 1 Task 1", uid=1, priority=1, period=3,
                                deadline=datetime.datetime.strptime("Sep 1 2018  10:00PM", '%b %d %Y %I:%M%p'))
        models_ctrl.task_create("User 2 Task 2", uid=2, priority=1, period=3,
                                deadline=datetime.datetime.strptime("Sep 1 2018  10:00PM", '%b %d %Y %I:%M%p'))
        models_ctrl.task_create("User 3 Task 3", uid=3, priority=2, period=1, deadline=None)
        models_ctrl.task_create("User 4 Task 4", uid=4, priority=3, period=4, deadline=None)
        models_ctrl.task_create("User 1 Task 5", uid=1, priority=0, period=0, deadline=None)
        models_ctrl.task_create("User 1 Task 6", uid=1, priority=0, period=0, deadline=None)
        models_ctrl.task_create("User 1 Task 7", uid=1, priority=0, period=0, deadline=None)
        models_ctrl.task_create("User 1 Task 8", uid=1, priority=0, period=0, deadline=None)
        self.user_1 = models_ctrl.user_get_by_login("User 1")
        self.user_2 = models_ctrl.user_get_by_login("User 2")
        self.user_3 = models_ctrl.user_get_by_login("User 3")
        self.user_4 = models_ctrl.user_get_by_login("User 4")
        self.task_1 = models_ctrl.task_get_by_tid(1, 1)
        self.task_2 = models_ctrl.task_get_by_tid(2, 2)
        self.task_3 = models_ctrl.task_get_by_tid(3, 3)
        self.task_4 = models_ctrl.task_get_by_tid(4, 4)
        self.task_5 = models_ctrl.task_get_by_tid(5, 1)
        self.task_6 = models_ctrl.task_get_by_tid(6, 1)
        self.task_7 = models_ctrl.task_get_by_tid(7, 1)
        self.task_8 = models_ctrl.task_get_by_tid(8, 1)

    def tearDown(self):
        storage_ctrl.user_delete(self.user_1.uid)
        storage_ctrl.user_delete(self.user_2.uid)
        storage_ctrl.user_delete(self.user_3.uid)
        storage_ctrl.user_delete(self.user_4.uid)
        storage_ctrl.task_delete(self.task_1.tid)
        storage_ctrl.task_delete(self.task_2.tid)
        storage_ctrl.task_delete(self.task_3.tid)
        storage_ctrl.task_delete(self.task_4.tid)
        storage_ctrl.task_delete(self.task_5.tid)
        storage_ctrl.task_delete(self.task_6.tid)
        storage_ctrl.task_delete(self.task_7.tid)
        storage_ctrl.task_delete(self.task_8.tid)

    def test_task_logic(self):
        with self.assertRaises(error.LoginReserved):
            models_ctrl.user_create("User 1", "1111")

        with self.assertRaises(error.WrongPassword):
            models_ctrl.user_edit(1, "2222", "User 1", "9999")
            
        models_ctrl.user_edit(1, "1111", "User 1", "9999")
        user = models_ctrl.user_get_by_login("User 1")
        self.assertEqual(user.password, "9999")
        self.assertEqual(len(models_ctrl.user_get_all()), 4)
        self.assertEqual(models_ctrl.user_is_exists("User 1"), True)
        self.assertEqual(models_ctrl.user_is_exists("User 5"), False)
        
        models_ctrl.user_edit(1, "9999", "User 5", "5555")
        user = models_ctrl.user_get_by_login("User 5")
        self.assertEqual(user.password, "5555")
        self.assertEqual(models_ctrl.user_is_exists("User 1"), False)
        self.assertEqual(models_ctrl.user_is_exists("User 5"), True)

        with self.assertRaises(error.NotOwner):
            models_ctrl.task_edit(tid=1, uid=2, title=None, priority=0, period=0, deadline=None)
        
        models_ctrl.task_edit(tid=1, uid=1, title=None, priority=0, period=0, deadline=None)
        
        task = models_ctrl.task_get_by_tid(1, 1)
        
        self.assertEqual(task.priority, self.task_1.priority)
        self.assertEqual(task.title, self.task_1.title)
        self.assertEqual(task.period, self.task_1.period)
        self.assertEqual(task.deadline, self.task_1.deadline)
        
        models_ctrl.task_clear(tid=1, uid=1, priority=True, period=True, deadline=True)

        task = models_ctrl.task_get_by_tid(1, 1)

        self.assertEqual(task.priority, 0)
        self.assertEqual(task.period, 0)
        self.assertIsNone(task.deadline)

        with self.assertRaises(error.NotOwner):
            models_ctrl.task_prolong(tid=1, uid=2)

        self.assertEqual(models_ctrl.task_prolong(tid=1, uid=1), False)
        
        models_ctrl.task_edit(tid=1, uid=1, title=None, priority=0, period=4,
                              deadline=datetime.datetime.strptime("Sep 1 2018  10:00PM", '%b %d %Y %I:%M%p'))

        models_ctrl.task_get_by_tid(1, 1)
        self.assertEqual(models_ctrl.task_prolong(tid=1, uid=1), True)

        task = models_ctrl.task_get_by_tid(1, 1)
        self.assertEqual(task.deadline, datetime.datetime.strptime("Sep 1 2019  10:00PM", '%b %d %Y %I:%M%p'))
        self.assertEqual(models_ctrl.task_prolong(tid=1, uid=1), False)

        with self.assertRaises(error.NotMember):
            models_ctrl.task_complete(tid=1, uid=2)

        models_ctrl.task_complete(tid=1, uid=1)
        task = models_ctrl.task_get_by_tid(1, 1)
        self.assertEqual(task.is_completed, True)

        models_ctrl.task_edit(tid=1, uid=1)
        task = models_ctrl.task_get_by_tid(1, 1)
        self.assertEqual(task.is_completed, False)

    def test_member_logic(self):

        with self.assertRaises(error.NotOwner):
            models_ctrl.member_create(mid=2, tid=2, uid=1)

        with self.assertRaises(error.MemberAlreadyExists):
            models_ctrl.member_create(mid=2, tid=2, uid=2)

        models_ctrl.member_create(mid=2, tid=1, uid=1)

        with self.assertRaises(error.NotOwner):
            models_ctrl.task_edit(tid=1, uid=2)

        models_ctrl.task_complete(tid=1, uid=2)
        task = models_ctrl.task_get_by_tid(1, 1)
        self.assertEqual(task.is_completed, True)

        models_ctrl.task_edit(tid=1, uid=1)
        task = models_ctrl.task_get_by_tid(1, 1)
        self.assertEqual(task.is_completed, False)

        with self.assertRaises(error.NotOwner):
            models_ctrl.member_remove(mid=2, tid=1, uid=2)

        with self.assertRaises(error.MemberNotFound):
            models_ctrl.member_remove(mid=3, tid=1, uid=1)

        models_ctrl.member_remove(mid=2, tid=1, uid=1)

        with self.assertRaises(error.NotMember):
            models_ctrl.task_complete(tid=1, uid=2)

    def test_group_logic(self):

        with self.assertRaises(error.NotOwner):
            models_ctrl.group_create(tid=1, group_str="Group", uid=2)

        models_ctrl.group_create(tid=1, group_str="Group", uid=1)

        with self.assertRaises(error.GroupAlreadyExists):
            models_ctrl.group_create(tid=1, group_str="Group", uid=1)

        with self.assertRaises(error.NotOwner):
            models_ctrl.group_remove(tid=1, group_str="Group", uid=2)

        models_ctrl.group_remove(tid=1, group_str="Group", uid=1)

        with self.assertRaises(error.GroupNotFound):
            models_ctrl.group_remove(tid=1, group_str="Group", uid=1)

    def test_bind_logic(self):

        with self.assertRaises(error.NotOwner):
            models_ctrl.bind_create(parent_id=5, child_id=6, uid=2)

        models_ctrl.bind_create(parent_id=5, child_id=6, uid=1)

        with self.assertRaises(error.BindLoop):
            models_ctrl.bind_create(parent_id=6, child_id=5, uid=1)

        with self.assertRaises(error.BindAlreadyExists):
            models_ctrl.bind_create(parent_id=7, child_id=6, uid=1)

        models_ctrl.task_complete(tid=5, uid=1)
        task = models_ctrl.task_get_by_tid(5, 1)
        self.assertEqual(task.is_completed, True)

        task = models_ctrl.task_get_by_tid(6, 1)
        self.assertEqual(task.is_completed, True)

        models_ctrl.task_edit(tid=5, uid=1)
        task = models_ctrl.task_get_by_tid(5, 1)
        self.assertEqual(task.is_completed, False)

        task = models_ctrl.task_get_by_tid(6, 1)
        self.assertEqual(task.is_completed, False)

        with self.assertRaises(error.NotOwner):
            models_ctrl.bind_remove(parent_id=5, child_id=6, uid=2)

        with self.assertRaises(error.BindNotFound):
            models_ctrl.bind_remove(parent_id=5, child_id=8, uid=1)

        models_ctrl.bind_remove(parent_id=5, child_id=6, uid=1)

        models_ctrl.task_complete(tid=5, uid=1)
        task = models_ctrl.task_get_by_tid(5, 1)
        self.assertEqual(task.is_completed, True)

        task = models_ctrl.task_get_by_tid(6, 1)
        self.assertEqual(task.is_completed, False)

        models_ctrl.task_edit(tid=5, uid=1)
        task = models_ctrl.task_get_by_tid(5, 1)
        self.assertEqual(task.is_completed, False)

        task = models_ctrl.task_get_by_tid(6, 1)
        self.assertEqual(task.is_completed, False)


if __name__ == '__main__':
    unittest.main()
