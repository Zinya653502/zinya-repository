from setuptools import setup, find_packages

setup(
    name='console',
    version='0.0.7',
    packages=find_packages(),
    entry_points={
        'console_scripts': ['DeadLiner = console.view.parser:start']
    }
)
