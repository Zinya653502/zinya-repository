"""
This module contains instructions how to parse user requests from CLI.
"""
import console.controllers.input as input_ctrl
from library.controllers.config_controller import turn_on_cli_mode
import argparse

# registration
#
# login
#
# logout
#
# user:
#     edit
#     profile
#     list
#
# task:
#     create
#     edit
#     archive
#     complete
#     list
#     bind
#     unbind
#
#     add:
#         member
#         group
#
#     remove:
#         member
#         group
#


def start():
    """
    This method initiates parser
    Parser supports 19 user commands, handle them and send to input controller
    :return: void
    """
    turn_on_cli_mode()

    # parser init
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    # subparsers
    user = subparsers.add_parser('user', help="""Use this commands to manage your profile
    (Make sure to log in first)""")
    task = subparsers.add_parser('task', help='Use this commands to manage your tasks (Make sure to log in first)')

    # registration
    registration = subparsers.add_parser('registration', help='Use this command to create your profile'
                                                              ' (Make sure that session is clear)')
    registration.add_argument('-l', '--login', type=str, required=True, help='Enter your login')
    registration.add_argument('-p', '--password', type=str, required=True, help='and your password')
    registration.set_defaults(func=input_ctrl.registration)

    # login
    login = subparsers.add_parser('login', help='Use this command to login (Make sure that session is clear)')
    login.add_argument('-l', '--login', type=str, required=True, help='Enter your login')
    login.add_argument('-p', '--password', type=str, required=True, help='and your password')
    login.set_defaults(func=input_ctrl.login)

    # logout
    logout = subparsers.add_parser('logout', help='Use this command to logout')
    logout.set_defaults(func=input_ctrl.logout)

    # user
    subparsers = user.add_subparsers()

    # user edit
    user_edit = subparsers.add_parser('edit', help='Use this command to edit your profile')
    user_edit.add_argument('-v', '--verify', type=str, required=True, help='Enter your old password')
    user_edit.add_argument('-l', '--login', type=str, help='Enter your new login')
    user_edit.add_argument('-p', '--password', type=str, help='and your new password')
    user_edit.set_defaults(func=input_ctrl.user_edit)

    # user profile
    user_profile = subparsers.add_parser('profile', help='Use this command to see your profile')
    user_profile.set_defaults(func=input_ctrl.user_profile)

    # user list
    user_list = subparsers.add_parser('list', help='Use this command to see users list')
    user_list.set_defaults(func=input_ctrl.user_list)

    # task
    subparsers = task.add_subparsers()

    # task create
    task_create = subparsers.add_parser('create', help='Use this command to create a new task')
    task_create.add_argument('-tt', '--title', type=str, required=True, help='Enter task title')
    task_create.add_argument('-dl', '--deadline', type=str, help='Enter task deadline in format "Jun 1 2018  1:30PM"')
    task_create.add_argument('-per', '--period', type=int, help='Enter task periodicity (from 0 to 4)',
                             choices=range(0, 5))
    task_create.add_argument('-pri', '--priority', type=int, help='Enter task priority (from 0 to 3)',
                             choices=range(0, 4))
    task_create.set_defaults(func=input_ctrl.task_create)

    # task edit
    task_edit = subparsers.add_parser('edit', help='Use this command to edit your task')
    task_edit.add_argument('-ti', '--tid', type=int, required=True, help='Enter task ID')
    task_edit.add_argument('-tt', '--title', type=str, help='Enter new task title')
    task_edit.add_argument('-dl', '--deadline', type=str, help='Enter new task deadline in format "Jun 1 2018  1:30PM"')
    task_edit.add_argument('-per', '--period', type=int, help='Enter new task periodicity (from 0 to 4)',
                           choices=range(0, 5))
    task_edit.add_argument('-pri', '--priority', type=int, help='Enter new task priority (from 0 to 3)',
                           choices=range(0, 4))
    task_edit.set_defaults(func=input_ctrl.task_edit)

    # task get
    task_get = subparsers.add_parser('get', help='Use this command to get task by ID')
    task_get.add_argument('-t', '--tid', type=int, required=True, help='Enter task ID')
    task_get.set_defaults(func=input_ctrl.task_get)

    # task list
    task_list = subparsers.add_parser('list', help='Use this command to see filtered task list')
    task_list.add_argument('-rt', '--root', action='store_true', help='Toggle this to see only root tasks')
    own_coll = task_list.add_mutually_exclusive_group()
    own_coll.add_argument('-own', '--owner', action='store_true', help='Toggle this to see only your own tasks')
    own_coll.add_argument('-coll', '--collective', action='store_true', help='or this to see only tasks as member')
    pen_comp = task_list.add_mutually_exclusive_group()
    pen_comp.add_argument('-pen', '--pending', action='store_true', help='Toggle this to see only pending tasks')
    pen_comp.add_argument('-com', '--completed', action='store_true', help='or this to see only completed tasks')
    task_list.add_argument('-gr', '--group', type=str, help='Enter group to search tasks only from it')
    task_list.set_defaults(func=input_ctrl.task_list)

    # task prolong
    task_prolong = subparsers.add_parser('prolong', help='Use this command to prolong task')
    task_prolong.add_argument('-t', '--tid', type=int, required=True, help='Enter task ID')
    task_prolong.set_defaults(func=input_ctrl.task_prolong)
    
    # task bind
    task_bind = subparsers.add_parser('bind', help='Use this command to bind tasks')
    task_bind.add_argument('-p', '--parent', type=int, required=True, help='Enter parent task ID')
    task_bind.add_argument('-c', '--child', type=int, required=True, help='and child task ID')
    task_bind.set_defaults(func=input_ctrl.task_bind)

    # task unbind
    task_unbind = subparsers.add_parser('unbind', help='Use this command to unbind tasks')
    task_unbind.add_argument('-p', '--parent', type=int, required=True, help='Enter parent task ID')
    task_unbind.add_argument('-c', '--child', type=int, required=True, help='and child task ID')
    task_unbind.set_defaults(func=input_ctrl.task_unbind)

    # task archive
    task_archive = subparsers.add_parser('archive', help='Use this command to archive your task')
    task_archive.add_argument('-t', '--tid', type=int, required=True, help='Enter task ID')
    task_archive.set_defaults(func=input_ctrl.task_archive)

    # task clear
    task_clear = subparsers.add_parser('clear', help='Use this command to clear task fields')
    task_clear.add_argument('-t', '--tid', type=int, required=True, help='Enter task ID')
    task_clear.add_argument('-dl', '--deadline', action='store_true', help='Toggle this to clear task deadline')
    task_clear.add_argument('-per', '--period', action='store_true', help='Toggle this to clear task periodicity')
    task_clear.add_argument('-pri', '--priority', action='store_true', help='Toggle this to clear task priority')
    task_clear.set_defaults(func=input_ctrl.task_clear)

    # task complete
    task_complete = subparsers.add_parser('complete', help='Use this command to complete your task')
    task_complete.add_argument('-t', '--tid', type=int, help='Enter task ID', required=True)
    task_complete.set_defaults(func=input_ctrl.task_complete)

    # subparsers
    task_add = subparsers.add_parser('add', help='Use this commands to add , member or group to your task')
    task_remove = subparsers.add_parser('remove',
                                        help='Use this commands to remove , member or group from your task')

    # task add
    subparsers = task_add.add_subparsers()

    # task add member
    task_add_member = subparsers.add_parser('member', help='Use this command to add member to your task')
    task_add_member.add_argument('-t', '--tid', type=int, required=True, help='Enter task ID')
    task_add_member.add_argument('-u', '--uid', type=str, required=True, help='and user ID')
    task_add_member.set_defaults(func=input_ctrl.task_add_member)

    # task add group
    task_add_group = subparsers.add_parser('group', help='Use this command to add group to your task')
    task_add_group.add_argument('-t', '--tid', type=int, required=True, help='Enter task ID')
    task_add_group.add_argument('-g', '--group', type=str, required=True, help='and task group')
    task_add_group.set_defaults(func=input_ctrl.task_add_group)

    # task remove
    subparsers = task_remove.add_subparsers()

    # task remove member
    task_remove_member = subparsers.add_parser('member', help='Use this command to remove member from your task')
    task_remove_member.add_argument('-t', '--tid', type=int, required=True, help='Enter task ID')
    task_remove_member.add_argument('-u', '--uid', type=str, required=True, help='and user ID')
    task_remove_member.set_defaults(func=input_ctrl.task_remove_member)

    # task remove group
    task_remove_group = subparsers.add_parser('group', help='Use this command to remove group from your task')
    task_remove_group.add_argument('-t', '--tid', type=int, required=True, help='Enter task ID')
    task_remove_group.add_argument('-g', '--group', type=str, required=True, help='and task group')
    task_remove_group.set_defaults(func=input_ctrl.task_remove_group)

    # parse
    args = parser.parse_args()
    args.func(args)
