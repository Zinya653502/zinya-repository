"""
This module contains instructions how to respond on user requests
"""
import library.controllers.models_controller as models


def print_error_details(error):
    """
    Prints deadliner errors
    :param error: deadliner error
    :return: void
    """
    print_title("-ERROR-")
    print("[!] {} error with exit code {}".format(error.name, error.code))
    print("[!] {}".format(error.desc))


def print_title(message):
    """
    Print deadliner title and received message
    :param message: string
    :return: void
    """
    print("".center(32, "="))
    print("-DeadLiner-".center(32, " "))
    print(message.center(32, " "))
    print("".center(32, "="))


def print_task(task):
    """
    Print deadliner task
    :param task: deadliner task
    :return: void
    """
    if task.parent:
        parent = models.task_get_parent(task.tid)
        print("".center(32, "*"))
        print_task_short(parent)
        print("".center(32, "*"))

    if task.is_completed is True:
        print('Completed: {} - {}'.format(task.tid, task.title))
    else:
        print('Pending: {} - {}'.format(task.tid, task.title))
    user = models.user_get_by_uid(task.uid)
    print("Owner: {} - {}".format(user.uid, user.login))
    if task.priority:
        print("Priority: ", task.get_str_priority())
    else:
        print("Priority: Common")
    if task.deadline:
        print("Deadline: ", task.deadline.strftime('%b %d %Y %I:%M%p'))
        if task.period:
            print("Periodicity: ", task.get_str_period())
        else:
            print("Periodicity: Do not repeat")
    groups = models.task_get_groups(task.tid)
    if groups:
        group_list = []
        for group in groups:
            group_list.append(group.group)
        print("Groups: ", ", ".join(group_list))

    children = models.task_get_children(task.tid)
    if children:
        print("".center(32, "-"))
        for child in children:
            print_task_short(child)
        print("".center(32, "-"))

    members = models.task_get_members(task.tid)
    if members:
        print("".center(32, "~"))
        for member in members:
            print_member(member)
        print("".center(32, "~"))

    print("".center(32, "="))


def print_task_short(task):
    """
    Prints deadliner task shortly
    :param task: deadliner task
    :return: void
    """
    if task.is_completed is True:
        print('[V] {} - {}'.format(task.tid, task.title))
    else:
        print('[X] {} - {}'.format(task.tid, task.title))


def print_member(user):
    """
    Prints deadliner member
    :param user: deadliner user
    :return: void
    """
    print("[+] {} - {}".format(user.uid, user.login))


def print_user(user):
    """
    Prints deadliner user
    :param user: deadliner user
    :return: void
    """
    print("{} - {}".format(user.uid, user.login).center(32, " "))


def registration(user_login):
    """
    Prints deadliner registration message
    :param user_login: string
    :return: void
    """
    user = models.user_get_by_login(user_login)
    print_title("-Welcome!-")
    print("You created an account")
    print_user(user)
    print("Now you can login in application")


def login(user_login):
    """
    Prints deadliner login message
    :param user_login: string
    :return: void
    """
    print_title("-Hello, " + user_login + "-")
    print("You now logged in application".center(32, " "))


def logout():
    """
    Prints deadliner logout message
    :return: void
    """
    print_title("-Bye-Bye!-")


def user_edit(user_login):
    """
    Prints user edit message
    :param user_login: string
    :return: void
    """
    print_title("-Your profile updated-")
    print_user(models.user_get_by_login(user_login))


def user_profile():
    """
    Prints current user profile
    :return: void
    """
    print_title("-Your profile-")
    user = models.user_get_by_uid()
    if user:
        print_user(user)
    else:
        print("Make sure to log in first".center(32, " "))


def user_list():
    """
    Prints user list
    :return: void
    """
    print_title("-List of all users-")
    users = models.user_get_all()
    if users:
        print("".center(32, "~"))
        for user in users:
            print_user(user)
        print("".center(32, "~"))
    else:
        print("User list is empty".center(32, " "))


def task_create():
    """
    Prints task create message
    :return: void
    """
    print_title("-Task created-")


def task_edit(tid):
    """
    Prints task edit message
    :param tid: int -- task ID
    :return: void
    """
    print_title("-Your task updated-")
    print_task(models.task_get_by_tid(tid, uid=-1))


def task_archive(tid):
    """
    Prints task archive message
    :param tid: int -- task ID
    :return: void
    """
    print_title("-Task {} archived-".format(tid))
    print("Now it's unavailable".center(32, " "))


def task_clear(tid):
    """
    Prints task clear message
    :param tid: int -- task ID
    :return: void
    """
    print_title("-Clearing task {} fields-".format(tid))
    print_task(models.task_get_by_tid(tid))


def task_prolong(is_needed, tid):
    """
    Prints task prolong message
    :param is_needed: bool -- was task prolonged or does not
    :param tid: int -- task ID
    :return: void
    """
    if is_needed:
        print_title("-Task {} prolongated-".format(tid))
    else:
        print_title("-No need to prolong task {}-".format(tid))


def task_complete(tid):
    """
    Prints task complete message
    :param tid: int -- task ID
    :return: void
    """
    print_title("-Task {} accomplished-".format(tid))
    print_task(models.task_get_by_tid(tid, uid=-1))


def task_bind(pid, cid):
    """
    Prints task bind message
    :param pid: int -- parent task ID
    :param cid: int -- child task ID
    :return: void
    """
    print_title("-Binding {} --> {}-".format(cid, pid))


def task_unbind(pid, cid):
    """
    Prints task unbind message
    :param pid: int -- parent task ID
    :param cid: int -- child task ID
    :return: void
    """
    print_title("-Unbinding {} -x- {}-".format(cid, pid))


def task_get(tid):
    """
    Prints task get message
    :param tid: int -- task ID
    :return: void
    """
    print_title("-Task {}-".format(tid))
    print_task(models.task_get_by_tid(tid, uid=-1))


def task_list(tasks):
    """
    Prints task list
    :param tasks: deadliner tasks
    :return: void
    """
    print_title("-Your task list-")
    if tasks:
        for task in tasks:
            print_task(task)
    else:
        print("Nothing was found")
        print("Try to change your filter settings")


def task_add_member(tid):
    """
    Prints task add member message
    :param tid: int -- task ID
    :return:
    """
    print_title("-Member added-")
    print_task(models.task_get_by_tid(tid, uid=-1))


def task_add_group(tid, group):
    """
    Prints task add group message
    :param tid: int -- task ID
    :param group: string
    :return:
    """
    print_title("-Task {} added to {} group-".format(tid, group))
    print_task(models.task_get_by_tid(tid, uid=-1))


def task_remove_member(tid):
    """
    Prints task remove member message
    :param tid: int -- task ID
    :return:
    """
    print_title("-Member removed-")
    print_task(models.task_get_by_tid(tid, uid=-1))


def task_remove_group(tid, group):
    """
    Prints task remove group message
    :param tid: int -- task ID
    :param group: string
    :return:
    """
    print_title("-Task {} removed from {} group-".format(tid, group))
    print_task(models.task_get_by_tid(tid, uid=-1))
