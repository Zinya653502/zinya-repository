"""
This module receive arguments from parser and deliver them to program logic
"""

import console.controllers.output as output
from library.controllers.logger_controller import logger_get
import library.controllers.config_controller as cfg_ctrl
import library.controllers.models_controller as models
import library.models.error as error
import datetime


log = logger_get('input_ctrl')


def exception_interceptor(func):
    """
    This decorator catch and handle deadliner errors
    :param func: function
    :return: decorated function
    """

    def function_decorator(*args, **kwargs):

        try:
            func(*args, **kwargs)
        except error.deadlinerError as err:
            output.print_error_details(err)

    return function_decorator


def user_is_logged(func):
    """
    This decorator raises NeedAuthorization error if function requires logged user
    :param func: function
    :return: decorated function
    """

    def function_decorator(*args, **kwargs):
        if not session_is_clear():
            return func(*args, **kwargs)
        else:
            log.error('Failed: cant recognise user (make sure to login first)')
            raise error.NeedAuthorization

    return function_decorator

###############################################


def session_get_uid():
    """
    Returns session config info
    :return: config info (int -- uid)
    """
    uid, userlogin = cfg_ctrl.session_read()
    return uid


def session_clear():
    """
    Clears session config info
    :return: None
    """
    cfg_ctrl.session_clear()


def session_update(uid, userlogin):
    """
    Updates session config info
    :param uid: int -- user ID
    :param userlogin: str -- login
    :return: None
    """
    cfg_ctrl.session_update(uid, userlogin)


def session_is_clear():
    """
    Checks if user is logged
    :return: True if don't
    """
    uid = session_get_uid()
    if uid == 0:
        return True
    else:
        return False


def user_login(userlogin, password):
    """
    Login user into the system
    :param userlogin: str -- user login
    :param password: str -- user password
    :return: None
    """
    log.info('Trying to login user {} with {} password'.format(userlogin, password))
    if session_is_clear():
        if models.user_is_exists(userlogin):
            user = models.user_get_by_login(userlogin)
            if user.password == password:
                session_update(user.uid, user.login)
                log.info('Succeed')
            else:
                log.error('Failed: incorrect password')
                raise error.WrongPassword
        else:
            log.error('Failed: user with {} login does not exist'.format(userlogin))
            raise error.UserNotFound
    else:
        log.error('Failed: user {} already logged in (make sure to log out first)'.format(session_get_uid()))
        raise error.SessionReserved

###################################################


@exception_interceptor
def registration(args):
    """
    Deliver registration args to models and output controller
    :param args: command line arguments (login, password)
    :return: None
    """
    if not session_is_clear():
        log.error('Failed: user {} already logged in (make sure to log out first)'.format(session_get_uid()))
        raise error.SessionReserved
    userlogin = args.login
    password = args.password
    models.user_create(userlogin, password)
    output.registration(userlogin)


@exception_interceptor
def login(args):
    """
    Deliver login args to models and output controller
    :param args: command line arguments (login, password)
    :return: None
    """
    if not session_is_clear():
        log.error('Failed: user {} already logged in (make sure to log out first)'.format(session_get_uid()))
        raise error.SessionReserved
    userlogin = args.login
    password = args.password
    user_login(userlogin, password)
    output.login(userlogin)


@exception_interceptor
def logout(args):
    """
    Send request to clear session to models and output controller
    :param args: command line arguments (empty)
    :return: None
    """
    session_clear()
    output.logout()


@exception_interceptor
@user_is_logged
def user_edit(args):
    """
    Deliver user edit args to models and output controller
    :param args: command line arguments (verify, login, password)
    :return: None
    """
    verify = args.verify
    userlogin = args.login
    password = args.password
    models.user_edit(session_get_uid(), verify, userlogin, password)
    output.user_edit(userlogin)


@exception_interceptor
@user_is_logged
def user_profile(args):
    """
    Send request to show logged user profile to models and output controller
    :param args: command line arguments (empty)
    :return: None
    """
    output.user_profile()


@exception_interceptor
def user_list(args):
    """
    Send request to show user list to models and output controller
    :param args: command line arguments (empty)
    :return: None
    """
    output.user_list()


@exception_interceptor
@user_is_logged
def task_create(args):
    """
    Deliver task create args to models and output controller
    :param args: command line arguments (title, priority, period, deadline)
    :return: None
    """
    title = args.title
    priority = args.priority
    deadline = args.deadline
    period = args.period
    if deadline:
        try:
            deadline = datetime.datetime.strptime(deadline, '%b %d %Y %I:%M%p')
        except Exception:
            raise error.IncorrectDatetime
    models.task_create(title=title, uid=session_get_uid(), priority=priority, period=period, deadline=deadline)
    output.task_create()


@exception_interceptor
@user_is_logged
def task_edit(args):
    """
    Deliver task create args to models and output controller
    :param args: command line arguments (tid, title, priority, period, deadline)
    :return: None
    """
    tid = args.tid
    title = args.title
    priority = args.priority
    deadline = args.deadline
    period = args.period
    if deadline:
        try:
            deadline = datetime.datetime.strptime(deadline, '%b %d %Y %I:%M%p')
        except Exception:
            raise error.IncorrectDatetime
    models.task_edit(tid, session_get_uid(), title, priority, period, deadline)
    output.task_edit(tid)


@exception_interceptor
@user_is_logged
def task_get(args):
    """
    Deliver task get args to models and output controller
    :param args: command line arguments (tid)
    :return: None
    """
    tid = args.tid
    task = models.task_get_by_tid(tid, session_get_uid())
    output.task_get(task.tid)


@exception_interceptor
@user_is_logged
def task_archive(args):
    """
    Deliver task archive args to models and output controller
    :param args: command line arguments (tid)
    :return: None
    """
    tid = args.tid
    models.task_archive(tid, session_get_uid())
    output.task_archive(tid)


@exception_interceptor
@user_is_logged
def task_prolong(args):
    """
    Deliver task prolong args to models and output controller
    :param args: command line arguments (tid)
    :return: None
    """
    tid = args.tid
    output.task_prolong(models.task_prolong(tid, session_get_uid()), tid)


@exception_interceptor
@user_is_logged
def task_complete(args):
    """
    Deliver task complete args to models and output controller
    :param args: command line arguments (tid)
    :return: None
    """
    tid = args.tid
    models.task_complete(tid, session_get_uid())
    output.task_complete(tid)


@exception_interceptor
@user_is_logged
def task_clear(args):
    """
    Deliver task clear args to models and output controller
    :param args: command line arguments (tid, priority, deadline, period)
    :return: None
    """
    tid = args.tid
    priority = args.priority
    deadline = args.deadline
    period = args.period
    models.task_clear(tid=tid, uid=session_get_uid(), priority=priority, deadline=deadline, period=period)
    output.task_clear(tid)


@exception_interceptor
@user_is_logged
def task_add_member(args):
    """
    Deliver task add member args to models and output controller
    :param args: command line arguments (tid, uid)
    :return: None
    """
    tid = args.tid
    mid = args.uid
    models.member_create(mid, tid, session_get_uid())
    output.task_add_member(tid)


@exception_interceptor
@user_is_logged
def task_remove_member(args):
    """
    Deliver task remove member args to models and output controller
    :param args: command line arguments (tid, uid)
    :return: None
    """
    tid = args.tid
    mid = args.uid
    models.member_remove(mid, tid, session_get_uid())
    output.task_remove_member(tid)


@exception_interceptor
@user_is_logged
def task_add_group(args):
    """
    Deliver task add group args to models and output controller
    :param args: command line arguments (tid, group)
    :return: None
    """
    tid = args.tid
    group = args.group
    models.group_create(tid, group, session_get_uid())
    output.task_add_group(tid, group)


@exception_interceptor
@user_is_logged
def task_remove_group(args):
    """
    Deliver task remove group args to models and output controller
    :param args: command line arguments (tid, group)
    :return: None
    """
    tid = args.tid
    group = args.group
    models.group_remove(tid, group, session_get_uid())
    output.task_remove_group(tid, group)


@exception_interceptor
@user_is_logged
def task_list(args):
    """
    Deliver task list args to models and output controller
    :param args: command line arguments (owner, collective, pending, completed, group, root)
    :return: None
    """
    owner = args.owner
    collective = args.collective
    pending = args.pending
    completed = args.completed
    group = args.group
    root = args.root
    tasks = models.task_get_filtered(session_get_uid(), owner, collective, pending, completed, group, root)
    output.task_list(tasks)


@exception_interceptor
@user_is_logged
def task_bind(args):
    """
    Deliver task bind args to models and output controller
    :param args: command line arguments (parent tid, child tid)
    :return: None
    """
    parent = args.parent
    child = args.child
    models.bind_create(parent, child, session_get_uid())
    output.task_bind(parent, child)


@exception_interceptor
@user_is_logged
def task_unbind(args):
    """
    Deliver task unbind args to models and output controller
    :param args: command line arguments (parent tid, child tid)
    :return: None
    """
    parent = args.parent
    child = args.child
    models.bind_remove(parent, child, session_get_uid())
    output.task_unbind(parent, child)
