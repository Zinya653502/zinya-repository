from django import forms
from library.models import deadliner_models as models


class TaskForm(forms.Form):
    title = forms.CharField(label="Task title", max_length=64, required=True)
    priority = forms.ChoiceField(choices=[(key, value) for key, value in models.PRIORITY_STR_VALUES.items()],
                                 required=True)
    period = forms.ChoiceField(choices=[(key, value) for key, value in models.PERIOD_STR_VALUES.items()], required=True)
    deadline = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}), required=False,
                                   input_formats=['%Y-%m-%dT%H:%M'], localize=False)
    parent = forms.IntegerField(label="Parent ID", required=False)


class GroupForm(forms.Form):
    group = forms.CharField(label="Group title", max_length=16, required=True)


class MemberForm(forms.Form):
    member = forms.IntegerField(label="Member ID", required=True)
