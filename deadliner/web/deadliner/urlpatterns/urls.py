from deadliner import views
from django.conf.urls import url, include

app_name = 'deadliner'
urlpatterns = [
    url(r'^home/$', views.home, name='home'),
    url(r'^registration/$', views.registration, name='registration'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^task/', include('deadliner.urlpatterns.task_urls')),
    url(r'^user/', include('deadliner.urlpatterns.user_urls')),
]


