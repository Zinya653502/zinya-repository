from django.conf.urls import url
from deadliner import views

urlpatterns = [
    url(r"^profile/$", views.profile, name="profile"),
    url(r"^list/$", views.user_list, name="user_list"),
    url(r"^page/(?P<uid>[0-9]+)$", views.user_page, name="user_page"),
]
