from django.conf.urls import url
from deadliner import views

urlpatterns = [
    url(r"^create/$", views.add_task, name="add_task"),
    url(r"^details/(?P<tid>[0-9]+)$", views.show_task, name="show_task"),
    url(r"^update/(?P<tid>[0-9]+)$", views.task_update, name="task_update"),
    url(r"^archive/(?P<tid>[0-9]+)$", views.task_archive, name="task_archive"),
    url(r"^complete/(?P<tid>[0-9]+)$", views.task_complete, name="task_complete"),
    url(r"^prolong/(?P<tid>[0-9]+)$", views.task_prolong, name="task_prolong"),
    url(r"^children/(?P<tid>[0-9]+)$", views.task_children, name="task_children"),
    url(r"^group/(?P<tid>[0-9]+)$", views.add_group, name="add_group"),
    url(r"^member/(?P<tid>[0-9]+)$", views.add_member, name="add_member"),
    url(r"^group_rm/(?P<gid>[0-9]+)$", views.group_remove, name="group_remove"),
    url(r"^member_rm/(?P<tid>[0-9]+)/(?P<mid>[0-9]+)$", views.member_remove, name="member_remove"),
    url(r"^tid/$", views.task_get_by_tid, name="task_get_by_tid"),
    url(r"^bind/$", views.task_bind, name="task_bind"),
    url(r"^search/$", views.advanced_search, name="advanced_search"),
]
