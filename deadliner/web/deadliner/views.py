from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect
from django.urls import reverse

from library.controllers import storage_controller as storage_ctrl
from library.controllers import models_controller as models_ctrl
from library.models.deadliner_models import Task, User, PRIORITY_STR_VALUES, PERIOD_STR_VALUES, Group, Member
from deadliner.forms import TaskForm, GroupForm, MemberForm
import library.models.error as error

import datetime


def registration(request):
    if request.method == 'POST':
        try:
            user_form = UserCreationForm(request.POST)
            user_form.save()

            if user_form.is_valid():
                username = user_form.cleaned_data['username']
                password = user_form.cleaned_data['password2']

                user = authenticate(username=username, password=password)
                auth_login(request, user)
                storage_ctrl.user_add(User(uid=user.id, login=username, password=password,
                                           creation_time=datetime.datetime.now()))
                return HttpResponseRedirect(reverse('deadliner:home'))
        except ValueError:
            pass

    user_form = UserCreationForm()
    context = {'user_form': user_form}

    return render(request=request, template_name='profile/registration.html', context=context)


def login(request):
    context = {}
    if request.method == 'POST':
        try:
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                auth_login(request, user)
                return HttpResponseRedirect(reverse('deadliner:home'))
            else:
                context['message'] = 'Invalid login'
        except Exception:
            context['message'] = 'Unknown error'
    return render(request=request, template_name='profile/login.html', context=context)


def logout(request):
    auth_logout(request)
    return HttpResponseRedirect(reverse('deadliner:login'))


@login_required(login_url='/deadliner/login/')
def task_bind(request):
    uid = request.user.id

    parent = request.POST['parent']
    child = request.POST['child']

    try:
        parent = models_ctrl.task_get_by_tid(parent, uid)
        child = models_ctrl.task_get_by_tid(child, uid)
    except error.TaskNotFound:
        return page404()

    if child.parent == parent.tid:
        try:
            models_ctrl.bind_remove(parent.tid, child.tid, uid)
        except error.deadlinerError:
            return page403("You can unbind only your tasks, which are already binded")
        return HttpResponseRedirect(reverse('deadliner:home'))
    else:
        try:
            models_ctrl.bind_create(parent.tid, child.tid, uid)
        except error.deadlinerError:
            return page403("You can unbind only your tasks, which are unbinded")
        return HttpResponseRedirect(reverse('deadliner:home'))


@login_required(login_url='/deadliner/login/')
def task_get_by_tid(request):
    uid = request.user.id

    tid = request.POST['tid']

    try:
        task = models_ctrl.task_get_by_tid(tid, uid)
    except error.TaskNotFound:
        return page404()
    except error.NotMember:
        return page403("This task is not yours or shared with you")

    members = storage_ctrl.task_get_members(tid)
    for member in members:
        member.tid = task.tid
    groups = storage_ctrl.task_get_groups(tid)
    children = storage_ctrl.task_get_children(tid)
    parent = storage_ctrl.task_get_parent(tid)

    task.priority = PRIORITY_STR_VALUES.get(task.priority)
    task.period = PERIOD_STR_VALUES.get(task.period)
    if models_ctrl.task_get_children(task.tid):
        task.children = True

    context = {'task': task, 'children': children, 'parent': parent,
               'members': members, 'groups': groups, 'group_form': GroupForm, 'member_form': MemberForm}

    return render(request, 'task/details.html', context=context)


@login_required(login_url='/deadliner/login/')
def show_task(request, tid):
    uid = request.user.id
    user = models_ctrl.user_get_by_uid(uid)

    try:
        task = models_ctrl.task_get_by_tid(tid, uid)
    except error.TaskNotFound:
        return page404()
    except error.NotMember:
        return page403("This task is not yours or shared with you")

    members = storage_ctrl.task_get_members(tid)
    for member in members:
        member.tid = task.tid
        member.own = task.uid
    groups = storage_ctrl.task_get_groups(tid)
    for group in groups:
        group.uid = task.uid
    children = storage_ctrl.task_get_children(tid)
    parent = storage_ctrl.task_get_parent(tid)

    task.priority = PRIORITY_STR_VALUES.get(task.priority)
    task.period = PERIOD_STR_VALUES.get(task.period)
    if models_ctrl.task_get_children(task.tid):
        task.children = True

    task.user = models_ctrl.user_get_by_uid(task.uid).login

    context = {'task': task, 'children': children, 'parent': parent,
               'members': members, 'groups': groups, 'group_form': GroupForm, 'member_form': MemberForm}

    return render(request, 'task/details.html', context=context)


@login_required(login_url='/deadliner/login/')
def advanced_search(request):
    uid = request.user.id
    own = request.POST.get('own', False) == 'on'
    collective = request.POST.get('collective', False) == 'on'
    completed = request.POST.get('completed', False) == 'on'
    pending = request.POST.get('pending', False) == 'on'
    root = request.POST.get('root', False) == 'on'
    group = request.POST.get('group', None)

    tasks = models_ctrl.task_get_filtered(uid, own, collective, pending, completed, group, root)

    context = {'tasks': tasks, 'after_search': True}
    return render(request, 'home.html', context=context)


@login_required(login_url='/deadliner/login/')
def task_children(request, tid):
    tasks = models_ctrl.task_get_children(tid)
    for task in tasks:
        if models_ctrl.task_get_children(task.tid):
            task.children = True
    context = {'tasks': tasks}
    return render(request, 'home.html', context=context)


@login_required(login_url='/deadliner/login/')
def home(request):
    uid = request.user.id
    tasks = models_ctrl.task_get_filtered(uid=uid)
    for task in tasks:
        if models_ctrl.task_get_children(task.tid):
            task.children = True

    context = {'tasks': tasks}
    return render(request, 'home.html', context=context)


@login_required(login_url='/deadliner/login/')
def user_page(request, uid):
    user = models_ctrl.user_get_by_uid(uid)
    if not user:
        return page404()

    context = {'user': user}

    return render(request, 'profile/profile.html', context=context)


@login_required(login_url='/deadliner/login/')
def profile(request):
    uid = request.user.id
    user = models_ctrl.user_get_by_uid(uid)
    if not user:
        return page404()

    context = {'user': user}

    return render(request, 'profile/profile.html', context=context)


@login_required(login_url='/deadliner/login/')
def user_list(request):

    users = models_ctrl.user_get_all()

    context = {'users': users}

    return render(request, 'profile/list.html', context=context)


@login_required(login_url='/deadliner/login/')
def add_task(request):
    uid = request.user.id

    task_form = TaskForm(request.POST)
    if task_form.is_valid():
        task = Task(title=task_form.cleaned_data['title'], uid=uid, priority=task_form.cleaned_data['priority'],
                    period=task_form.cleaned_data['period'], deadline=task_form.cleaned_data['deadline'],
                    creation_time=datetime.datetime.now(),
                    is_completed=False, is_active=True)

        storage_ctrl.task_add(task)

        return HttpResponseRedirect(reverse('deadliner:show_task', args=[task.tid]))

    context = {'task_form': task_form}
    return render(request=request, template_name="task/update.html", context=context)


@login_required(login_url='/deadliner/login/')
def add_group(request, tid):
    uid = request.user.id
    group_form = GroupForm(request.POST)
    if group_form.is_valid():

        models_ctrl.group_create(tid, group_form.cleaned_data['group'], uid)
        return HttpResponseRedirect(reverse('deadliner:show_task', args=[tid]))
    context = {'group_form': group_form}
    return render(request=request, template_name="task/update.html", context=context)


@login_required(login_url='/deadliner/login/')
def add_member(request, tid):
    uid = request.user.id
    member_form = MemberForm(request.POST)
    if member_form.is_valid():

        models_ctrl.member_create(member_form.cleaned_data['member'], tid, uid)
        return HttpResponseRedirect(reverse('deadliner:show_task', args=[tid]))
    context = {'member_form': member_form}
    return render(request=request, template_name="task/update.html", context=context)


@login_required(login_url='/deadliner/login/')
def task_update(request, tid):
    uid = request.user.id
    task = models_ctrl.task_get_by_tid(tid, uid)
    if not task:
        return page404()

    task_form = TaskForm(request.POST)

    if task_form.is_valid():

        models_ctrl.task_edit(tid, uid, title=task_form.cleaned_data['title'],
                              priority=task_form.cleaned_data['priority'],
                              period=task_form.cleaned_data['period'], deadline=task_form.cleaned_data['deadline'])

        priority = period = deadline = False
        if not task_form.cleaned_data['priority']:
            priority = True
        if not task_form.cleaned_data['period']:
            period = True
        if not task_form.cleaned_data['deadline']:
            deadline = True

        models_ctrl.task_clear(tid, uid, priority, period, deadline)

        return HttpResponseRedirect(reverse('deadliner:show_task', args=[task.tid]))

    context = {'task': task, 'task_form': task_form}
    return render(request=request, template_name='task/update.html', context=context)


@login_required(login_url='/deadliner/login/')
def task_archive(request, tid):
    models_ctrl.task_archive(tid, request.user.id)
    return HttpResponseRedirect(reverse('deadliner:home'))


@login_required(login_url='/deadliner/login/')
def group_remove(request, gid):
    storage_ctrl.group_delete(gid)
    return HttpResponseRedirect(reverse('deadliner:home'))


@login_required(login_url='/deadliner/login/')
def member_remove(request, tid, mid):
    uid = request.user.id
    models_ctrl.member_remove(mid, tid, uid)
    return HttpResponseRedirect(reverse('deadliner:show_task', args=[tid]))


@login_required(login_url='/deadliner/login/')
def task_prolong(request, tid):
    models_ctrl.task_prolong(tid, request.user.id)
    return HttpResponseRedirect(reverse('deadliner:show_task', args=[tid]))


@login_required(login_url='/deadliner/login/')
def task_complete(request, tid):
    try:
        models_ctrl.task_complete(tid, request.user.id)
    except error.TaskNotFound:
        return page404()
    except error.NotMember:
        return page403("This task is not yours or shared with you")

    return HttpResponseRedirect(reverse('deadliner:home'))


def page404(message=None):
    context = {}
    if message:
        context["message"] = message
    response = render_to_response('errors/404.html', context=context, status=404)
    return response


def page403(message=None):
    context = {}
    if message:
        context["message"] = message
    response = render_to_response('errors/403.html', context=context, status=404)
    return response
